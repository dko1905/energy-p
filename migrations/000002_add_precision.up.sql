ALTER TABLE prices
ADD COLUMN price_value_precision INTEGER NOT NULL DEFAULT 3;
