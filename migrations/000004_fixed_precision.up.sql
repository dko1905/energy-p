ALTER TABLE prices RENAME COLUMN price_value TO price_value_old;
ALTER TABLE prices ADD COLUMN price_value TEXT NOT NULL DEFAULT "";
UPDATE prices SET price_value=CAST(CAST(price_value_old AS FLOAT) / 10000 AS TEXT);
ALTER TABLE prices DROP COLUMN price_value_old;
