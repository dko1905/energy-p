CREATE TABLE prices(
  price_id TEXT PRIMARY KEY,
  price_ts DATETIME NOT NULL,
  price_fetch_ts DATETIME NOT NULL,
  price_value INTEGER NOT NULL
);
