{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    gomod2nix.url = "github:nix-community/gomod2nix";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils, gomod2nix }:

  flake-utils.lib.eachDefaultSystem (system:
    let
      pkgs = import nixpkgs {
        inherit system;
        overlays = [ gomod2nix.overlays.default ];
      };
    in
    rec {
      devShell = pkgs.mkShellNoCC {
        name = "go";

        buildInputs = with pkgs; [
          go
          delve
          gopls
          go-tools
          pkgs.gomod2nix
        ];
      };

      packages.energyP = pkgs.buildGoApplication {
        pname = "energy-p";
        version = "0.0.1";
        pwd = ./.;
        src = ./.;
        modules = ./gomod2nix.toml;
        tags = ["sqlite_math_functions"];
      };

      packages.energyPImage = pkgs.dockerTools.buildImage {
        name = "energyp";
        tag = "latest";
        created = "now";
        copyToRoot = pkgs.buildEnv {
          name = "image-root";
          paths = [ packages.energyP pkgs.busybox ];
          pathsToLink = [ "/bin" ];
        };
        config.Cmd = [ "/bin/energy-p" ];
      };

      defaultPackage = packages.energyPImage;
    }
  );
}
