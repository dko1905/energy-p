package main

import (
	"embed"
	"encoding/csv"
	"encoding/json"
	"fmt"
	"net/http"
	"regexp"
	"time"

	"github.com/gocolly/colly"
	"github.com/goodsign/monday"
	"github.com/relvacode/iso8601"
	"github.com/shopspring/decimal"
	log "github.com/sirupsen/logrus"

	"github.com/go-co-op/gocron"
	"github.com/golang-migrate/migrate/v4"
	migrate_sqlite3 "github.com/golang-migrate/migrate/v4/database/sqlite3"
	_ "github.com/golang-migrate/migrate/v4/source/file"
	"github.com/golang-migrate/migrate/v4/source/iofs"
	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"
	_ "github.com/mattn/go-sqlite3"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/gofiber/fiber/v2/middleware/monitor"
)

type EnergyPrice struct {
	ID             uuid.UUID       `db:"price_id"`
	TS             time.Time       `db:"price_ts"`
	FetchTS        time.Time       `db:"price_fetch_ts"`
	Value          decimal.Decimal `db:"price_value"`
	ValuePrecision int64           `db:"price_value_precision"`
}

//go:embed migrations/*.sql
var migrationFs embed.FS

func main() {
	// Configure logging
	log.SetLevel(log.DebugLevel)

	// Create app
	app := fiber.New(fiber.Config{
		StrictRouting: true,
		AppName:       "Energy-P",
	})

	// Open db
	db, err := sqlx.Open("sqlite3", "./database.db")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	// Create scheduler
	sch := gocron.NewScheduler(time.UTC)
	configureScheduler(db, sch)

	// Create routes
	configureMiddleware(db, app)
	app.Get("/", func(c *fiber.Ctx) error {
		return c.SendString("OK")
	})
	configureApi(db, app.Group("/api"))

	// Migrate database
	migrateDatabase(db)

	// Start schedular
	sch.StartAsync()

	// Run
	app.Listen(":3000")
}

func migrateDatabase(db *sqlx.DB) {
	log.Info("Running migrations...")
	d, err := iofs.New(migrationFs, "migrations")
	if err != nil {
		panic(err)
	}
	driver, err := migrate_sqlite3.WithInstance(db.DB, &migrate_sqlite3.Config{})
	if err != nil {
		log.Fatal(err)
	}
	m, err := migrate.NewWithInstance(
		"iofs", d,
		"sqlite3", driver,
	)
	if err != nil {
		log.Fatal(err)
	}
	err = m.Migrate(4)
	if err == migrate.ErrNoChange {
		log.Info("No database changes")
	} else if err != nil {
		log.Fatal(err)
	} else {
		log.Info("Migrated successfully")
	}
}

func saveScrapedPrices(db *sqlx.DB, prices []EnergyPrice) (int, error) {
	// Save data to db
	tx := db.MustBegin()
	defer tx.Rollback()

	firstDate := prices[0].TS
	lastDate := prices[len(prices)-1].TS

	// Fetch existing prices
	existingPrices := []EnergyPrice{}
	// Array containing last found price in db
	err := db.Select(&existingPrices, "SELECT * FROM prices WHERE price_ts BETWEEN ? AND ? ORDER BY price_fetch_ts ASC;", firstDate, lastDate)
	if err != nil {
		return 0, err
	}

	saved_prices := 0
	for _, price := range prices {
		// Go each existing price and keep track if the latest found value
		// is conflicted.
		conflictExists := false
		recordExists := false
		for _, existingPrice := range existingPrices {
			if price.TS == existingPrice.TS {
				recordExists = true
				if !price.Value.Equal(existingPrice.Value) {
					conflictExists = true
				} else {
					conflictExists = false
				}
			}
		}
		if recordExists && !conflictExists {
			continue
		}

		stmt := `INSERT INTO prices (price_id, price_ts, price_fetch_ts, price_value, price_value_precision) VALUES (?, ?, ?, ?, ?);`
		r, err := tx.Exec(stmt, price.ID, price.TS, price.FetchTS, price.Value, price.ValuePrecision)
		if err != nil {
			tx.Rollback()
			return 0, err
		}
		saved_prices++
		_ = r
	}
	tx.Commit()
	return saved_prices, nil
}

func scrapeDataVersion1(db *sqlx.DB) {
	log.Info("Fetching data from price API")

	start := "2023-01-01"
	stop := "2023-01-07"
	url := fmt.Sprintf("https://andelenergi.dk/?obexport_format=csv&obexport_start=%s&obexport_end=%s&obexport_region=east", start, stop)

	resp, err := http.Get(url)
	if err != nil {
		log.Error(err)
		return
	}
	defer resp.Body.Close()
	fetch_ts := time.Now().UTC() // Fetch time

	csvReader := csv.NewReader(resp.Body)
	data, err := csvReader.ReadAll()
	if err != nil {
		log.Error(err)
		return
	}

	prices := []EnergyPrice{}

	for i, row := range data {
		if i == 0 {
			if row[0] != "Date" {
				log.Error("Returned data does not match expected 'Date'")
				return
			}
		} else {
			if len(row) != 25 {
				log.Errorf("Row length does not match %v instead of %v\n", len(row), 24)
				return
			}
			// Parse date
			date, err := iso8601.ParseString(row[0])
			if err != nil {
				log.Error(err)
				return
			}

			// Parse prices
			newPricesThisDay := 0
			for n := 1; n < 25; n++ {
				if row[n] == "" {
					continue
				} else {
					price := EnergyPrice{}
					price.ID = uuid.New()
					price.TS = date.Add(time.Hour * time.Duration(n-1))
					price.FetchTS = fetch_ts
					if value, err := decimal.NewFromString(row[n]); err != nil {
						price.Value = value
					} else {
						log.Error(err)
						return
					}
					price.ValuePrecision = int64(price.Value.Exponent())
					prices = append(prices, price)
				}
				newPricesThisDay++
			}
			if newPricesThisDay == 0 {
				log.Warnf("Not new prices for %v\n", date)
			}
		}
	}

	// Check for no prices
	if len(prices) == 0 {
		log.Info("No prices fetched")
	}

	saved_prices, err := saveScrapedPrices(db, prices)
	if err != nil {
		log.Error(err)
		return
	}
	log.Infof("Saved %v of %v price information", saved_prices, len(prices))
}

func scrapeDataVersion2(db *sqlx.DB) {
	log.Info("Fetching data from price API")

	c := colly.NewCollector()
	fetch_ts := time.Now().UTC() // Fetch time

	prices := []EnergyPrice{}

	parseAndSaveFn := func(dataStr string) {
		type ScrapeDataV2 struct {
			East struct {
				Labels             []string `json:"labels"`
				Values             []string `json:"values"`
				ValuesDistribution []string `json:"valuesDistribution"`
				Dates              []struct {
					Date string `json:"date"`
					Day  string `json:"day"`
				} `json:"dates"`
				Time int64 `json:"time"`
			} `json:"east"`
		}

		// Parse data as json
		var data ScrapeDataV2
		err := json.Unmarshal([]byte(dataStr), &data)
		if err != nil {
			log.Error(err)
			return
		}

		// TODO: Assert data is correct

		// Do data
		east := data.East
		dkLocation, err := time.LoadLocation("Europe/Copenhagen")
		if err != nil {
			panic(err)
		}
		currentDateEx := regexp.MustCompile("[0-9].*")

		// TODO: Everything breaks if the labels don't follow 00 01 02... 23 00 01...
		for i := range east.Labels {
			// Parse current date
			currentDateStr := currentDateEx.FindString(east.Dates[i/24].Date)
			currentDate, err := monday.ParseInLocation(
				"2. January", currentDateStr, dkLocation, monday.LocaleDaDK,
			)
			if err != nil {
				log.Error(err)
				continue
			}
			currentDate = currentDate.AddDate(time.Now().Year(), 0, 0)
			currentDate = currentDate.Add(time.Duration(i%24) * time.Hour)

			// Create price object
			price := EnergyPrice{}
			price.ID = uuid.New()
			price.TS = currentDate.UTC()
			price.FetchTS = fetch_ts
			if value, err := decimal.NewFromString(east.Values[i]); err == nil {
				price.Value = value
			} else {
				log.Error(err)
				return
			}
			price.ValuePrecision = int64(price.Value.Exponent() * -1)
			prices = append(prices, price)
		}

		// Check for no prices
		if len(prices) == 0 {
			log.Info("No prices fetched")
		}

		saved_prices, err := saveScrapedPrices(db, prices)
		if err != nil {
			log.Error(err)
			return
		}
		log.Infof("Saved %v of %v price information", saved_prices, len(prices))
	}

	c.OnRequest(func(r *colly.Request) {
		r.Headers.Set("User-Agent", "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/110.0.0.0 Safari/537.36")
	})

	c.OnHTML("#chart-component", func(e *colly.HTMLElement) {
		dataStr := e.Attr("data-chart")
		if dataStr == "" {
			log.Errorln("scrape: chart data is empty")
			return
		}

		parseAndSaveFn(dataStr)
	})

	c.Visit("https://andelenergi.dk/sel/timeenergi/")
}

func configureScheduler(db *sqlx.DB, sch *gocron.Scheduler) {
	sch.Every(12).Hours().Do(func() {
		scrapeDataVersion2(db)
	})
}

func configureMiddleware(db *sqlx.DB, app *fiber.App) {
	// Monitor
	app.Get("/metrics", monitor.New(monitor.Config{Title: "MyService Metrics Page"}))
	// CORS
	app.Use(cors.New(cors.Config{}))
}

func configureApi(db *sqlx.DB, api fiber.Router) {
	api.Get("/price", func(c *fiber.Ctx) error {
		prices := []EnergyPrice{}
		start, err := iso8601.ParseString(c.Query("start", ""))
		if err != nil {
			return err
		}
		end, err := iso8601.ParseString(c.Query("end", ""))
		if err != nil {
			return err
		}

		stmt := `SELECT * FROM prices WHERE price_ts BETWEEN ? AND ?;`
		err = db.Select(&prices, stmt, start, end)
		if err != nil {
			return err
		}

		return c.JSON(prices)
	})
	api.Post("/price", func(c *fiber.Ctx) error {
		return nil
	})
}
