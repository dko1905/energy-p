# Energy-P
Energy Power recorder.  


## Description
Saves energi price info every day, and exposes REST and web UI.


## Visuals
**INSERT IMAGE HERE**

## Installation
Golang

## License
[AGPL-3.0](./LICENSE.txt)
